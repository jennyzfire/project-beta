from django.urls import path

from .views import (
    api_list_sales,
    api_customer_form,
    api_employee_form,
    api_unsold_cars
    # api_sold_cars
)


urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    # path("sales/", api_list_sales, name="api_create_sales"),
    path("sales/customers/", api_customer_form, name="api_create_customer"),
    path("sales/employees/", api_employee_form, name="api_create_employee"),
    path("sales/unsoldcars/", api_unsold_cars, name="api_unsold_cars"),
    # path("sales/soldcars/<int:VIN>/", api_sold_cars, name="api_sold_cars"),
]
