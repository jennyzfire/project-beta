from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse



# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    VIN = models.CharField(max_length=200, unique=True)

    # def __str__(self):
    #     return {self.VIN}


class SalesPerson(models.Model):
    name = models.CharField(max_length=40)
    employee_number = models.CharField(max_length=100)

    # def __str__(self):
    #     return f"{self.employee_number}"


class Customer(models.Model):
    name = models.CharField(max_length=40)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)



class Sales(models.Model):
    # customer = models.CharField(max_length=200)
    price = models.CharField(max_length=200000)

    #This becomes VIN with a one to many relationship with the VIN in the AutomobileVO Above
    # Foreign key is a one to many
    VIN = models.ForeignKey(
        AutomobileVO,
        related_name="+",  # do not create a related name on LocationVO
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_delete_sales", kwargs={"pk": self.pk})

    salesPerson = models.ForeignKey(
        SalesPerson,
        related_name="salesPerson",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_delete_sales", kwargs={"pk": self.pk})

    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_delete_sales", kwargs={"pk": self.pk})
