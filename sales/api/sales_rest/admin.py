from django.contrib import admin
from .models import SalesPerson, Sales, Customer
# Register your models here.

admin.site.register(SalesPerson)
admin.site.register(Sales)
admin.site.register(Customer)
