from django.urls import path
from .views import list_all_car_vins, get_or_create_technician, list_or_create_appointments, delete_or_put_appointment

urlpatterns = [
    path("list/", list_all_car_vins, name="car_vins_url"),
    path("technicians/", get_or_create_technician, name="technicians_url"),
    path("appointments/", list_or_create_appointments, name="appointments_url"),
    path("appointments/<int:pk>/", delete_or_put_appointment, name="del_or_get_app_url")
]
