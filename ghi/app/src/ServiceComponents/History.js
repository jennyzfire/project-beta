import React, { useState, useEffect } from 'react';
import '../App.css';


function History() {

    const DATE_OPTIONS = {weekday: 'short', year: 'numeric', month: 'short', day: 'numeric'};
    const HOUR_OPTIONS = {hour: '2-digit', minute: '2-digit'};
    const [appointments, setAppointment] = useState([]);
    const [searchTerm, setSearchTerm] = useState()
    const [searchResults, setSearchResults] = useState([])

    function prettyHours(time){
        let date = new Date(parseInt(time));
        let thisTime = date.toLocaleTimeString('en-US', HOUR_OPTIONS);
        return thisTime
      }

    async function getAppointments() {
    let appointmentsUrl = "http://localhost:8080/cars/appointments/"
    try{
        const response = await fetch(appointmentsUrl);
        const listAppointments = await response.json();
        setAppointment(listAppointments);
        setSearchResults(listAppointments);
    } catch (e) {
        console.error(e);
    }
    }

    useEffect(
        () => {
          getAppointments();
        }, []
    )


    useEffect(
    () => {
        if(searchTerm){
        let filteredResults = appointments.filter(app => app.car_VIN.includes(searchTerm))
        console.log("FILTERED RESULTS:", filteredResults)
        setSearchResults(filteredResults);
        }
    }, [searchTerm]
    )

    return (
        <div className="app-list">
          <h1 className="form-name-s">Service History
          <input type="text" className="search-bar" placeholder="Search by vin..." onChange={(event) => {setSearchTerm(event.target.value)}}></input></h1>
          <div className="app-container">
              <table>
                  <thead>
                      <tr>
                          <th>Customer Name</th>
                          <th>VIP Status</th>
                          <th>VIN</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Technician</th>
                          <th>Reason</th>
                      </tr>
                  </thead>
                  <tbody>
                      {searchResults.map(app => {
                      return(
                      <tr key={app.id}>
                          <td>{app.customer}</td>
                          <td>{JSON.stringify(app.vip)}</td>
                          <td>{app.car_VIN}</td>
                          <td>{new Date(app.date).toLocaleDateString('en-US', DATE_OPTIONS)}</td>
                          <td>{prettyHours(app.date)}</td>
                          <td>{app.technician.name}</td>
                          <td>{app.reason}</td>
                      </tr>
                      )
                      })}
                  </tbody>
              </table>
          </div>
        </div>
      )
}

export default History
