import React, {useState, useRef, useEffect} from 'react'
import moment from 'moment';

function AppointmentForm() {
  const blankForm = {
    customer: "",
    car_VIN: "",
    date: "",
    technician: "",
    reason: ""
  }

  const [appointment, setAppointment] = useState(blankForm);
  const [technicians, setTechnician] = useState([]);

  function toHours(hours){
    let hour = moment(hours, "HH:mm");
    return hour
  }

  function toDate(date){
    let dates = moment(date, "YYYY-MM-DD");
    let hours = moment(date, "HH-mm");
    dates = dates.format("YYYY-MM-DD");
    toHours(hours);
    return dates
  }

  async function getTechnicians() {
    let listUrl = "http://localhost:8080/cars/technicians/"
    try {
      const response = await fetch(listUrl);
      const listOfTechnicians = await response.json();
      setTechnician(listOfTechnicians)
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(
    () => {
      getTechnicians();
    }, []
  )

  async function postingData(event) {
    event.preventDefault();
    const incomingData = {...appointment};
    const url = "http://localhost:8080/cars/appointments/"
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(incomingData),
      headers: {
          'Content-Type': 'appointment/json',
      }
    }

    const response = await fetch(url, fetchConfig);
    if(response.ok){
      const newAppointment = await response.json();
    }
  }

  const myForm = useRef(null);

  const resetState = (e) => {
      postingData(e);
      setAppointment(blankForm);
      myForm.current.reset();
  }

  return (
    <div className="flex-container">
      <div className="col-sm">
        <div className="form-container">
          <h1 className="form-name">Make Appointment</h1>
          <form ref={myForm} onSubmit={e => resetState(e)} className="rowsies">
              <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Customer Name</label>
                  <input defaultValue={appointment.customer} onChange={e => setAppointment({...appointment, customer: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="Enter name"/>
              </div>
              <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Car VIN</label>
                  <input defaultValue={appointment.car_VIN} onChange={e => setAppointment({...appointment, car_VIN: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="#"/>
              </div>
              <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Date</label>
                  <input defaultValue={appointment.date} onChange={e => setAppointment({...appointment, date: toDate(e.target.value)})} type="date" className="form-control" id="tech-form1" placeholder="#"/>
              </div>
              <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Time</label>
                  <input defaultValue={appointment.date} onChange={e => setAppointment({...appointment, date: toHours(e.target.value)})} type="time" className="form-control" id="tech-form1" placeholder="#"/>
              </div>
              <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Technician</label>
                  <select id="tech-form1" className="form-control" onChange={e => setAppointment({...appointment, technician: e.target.value})}>
                  {technicians.map(technician => {
                    return(
                      <option id="tech-form1" key={technician.id}>{technician.name}</option>
                    )
                  })}
                  </select>
              </div>
              <div className="form-group">
                  <label id="tech-form" htmlFor="exampleInputPassword1">Reason</label>
                  <input defaultValue={appointment.reason} onChange={e => setAppointment({...appointment, reason: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="Reason"/>
              </div>
              <button type="submit" id="submit-button" className="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
      </div>
  )
}

export default AppointmentForm
