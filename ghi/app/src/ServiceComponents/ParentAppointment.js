import React from 'react'
import AppointmentForm from './AppointmentForm'
import AppointmentList from './AppointmentList'
import '../App.css'


function ParentAppointment() {
  return (
    <div className="flex-container">
        <AppointmentForm/>
        <AppointmentList/>
    </div>
  )
}

export default ParentAppointment
