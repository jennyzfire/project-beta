import React, {useState, useEffect, useRef} from 'react'

function FormModel() {
    const blankForm = {
        manufacturer: "",
        name: "",
        picture_url: ""
    }

    const [manufacturers, setManufacturer] = useState([]);
    const [model, setModel] = useState(blankForm)

    async function getManufacturers() {
        const listUrl = "http://localhost:8100/api/manufacturers/";
        try {
          const response = await fetch(listUrl);
          const listOfManufacturers = await response.json();
          setManufacturer(listOfManufacturers.manufacturers);
        } catch (e) {
          console.error(e);
        }
      }

    async function postingData(event) {
        event.preventDefault();
        const incomingData = {...model};
        const url = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(incomingData),
            headers: {
                'Content-Type': 'appointment/json',
            }
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            const newModel = await response.json();
            console.log("New model:", newModel)
        }
    }

    const myForm = useRef(null);

    const resetState = (e) => {
        postingData(e);
        setModel(blankForm);
        myForm.current.reset();
    }

    useEffect(
    () => {
        getManufacturers();
    }, []
    )


  return (
    <div>
        <div className="form-container">
          <h1 className="form-name">Add Model</h1>
          <form ref={myForm} onSubmit={e => resetState(e)} className="rowsies">
              <div className="form-group">
                  <label>Manufacturer</label>
                  <select onChange={e => setModel({...model, manufacturer: e.target.value})} >
                    {manufacturers.map(manufacturer => {
                        return(
                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                        )
                    })}
                  </select>
              </div>
              <div className="form-group">
                  <label>Model</label>
                  <input defaultValue={model.name} onChange={e => setModel({...model, name: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="Model Name..."/>
              </div>
              <div className="form-group">
                  <label>Image/URL</label>
                  <input defaultValue={model.picture_url} onChange={e => setModel({...model, picture_url: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="Image URL..."/>
              </div>
              <button type="submit" id="submit-button" className="btn btn-primary">Submit</button>
              </form>
            </div>
    </div>
  )
}

export default FormModel
