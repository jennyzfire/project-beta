import React, {useEffect, useState} from 'react'
import FormManufacturer from './FormManufacturer';


function ManufacturerList() {

    const [manufacturers, setManufacturer] = useState([]);

    async function getManufacturers() {
        let listUrl = "http://localhost:8100/api/manufacturers/";
        try {
          const response = await fetch(listUrl);
          const listOfManufacturers = await response.json();
          setManufacturer(listOfManufacturers.manufacturers);
        } catch (e) {
          console.error(e);
        }
      }

      useEffect(
        () => {
            getManufacturers();
        }, []
      )

  return (
    <div>
        <FormManufacturer/>
        <table>
              <thead>
                  <tr>
                      <th>Manufacturer</th>
                      <th>ID</th>
                  </tr>
              </thead>
              <tbody>
                  {manufacturers.map(mans => {
                  return(
                  <tr key={mans.id}>
                      <td>{mans.name}</td>
                      <td>{mans.id}</td>
                  </tr>
                  )
                  })}
              </tbody>
          </table>
    </div>
  )
}

export default ManufacturerList
