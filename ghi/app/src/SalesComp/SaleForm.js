// You need to create a form that associates an automobile that came from inventory and has not yet been sold, a sales person,
// and a customer with a price to record the sale of an automobile. When the form is submitted, the sales record is stored in the application.
// You need to create a link in the navbar to get to the Create a sale record form.
// The following screenshot shows what your component could look like.
import React, { Component } from 'react'

class SaleForm extends React.Component { // this is inheriting the functionality of our react class 
    constructor(props) { // the props are like arguments and they are coming from the super class of react component
        super(props) // 
        this.state = {
            VINS: [],
            VIN: '',
            salesPersons: [], // Always do data delete on lists
            salesPerson: '',
            customers: [],
            customer: '',
            price: '',
        };
    }

    // Getting select data for each select tag
    async componentDidMount() { // component did mount is a method which exists in the React component.
        const unsoldcars_url = "http://localhost:8090/api/sales/unsoldcars/";
        const customer_url = "http://localhost:8090/api/sales/customers/";
        const salesPerson_url = "http://localhost:8090/api/sales/employees/";
        const unsoldcars_response = await fetch(unsoldcars_url);
        const customer_response = await fetch(customer_url);
        const salesPerson_response = await fetch(salesPerson_url);
        if(unsoldcars_response.ok && customer_response.ok && salesPerson_response.ok) {
            const  unsoldcars = await unsoldcars_response.json();
            const  customer = await customer_response.json();
            const  salesPerson= await salesPerson_response.json();

            this.setState({
                VINS: unsoldcars.Automobiles,
                salesPersons: salesPerson.salesPerson,
                customers: customer.customer,
            })

        }
    }


        async handleSubmit(event) {
            event.preventDefault();

            const data = {...this.state} ; //copies all of the properties and values from this.sales into a new object (dictionary)
            delete data.VINS
            delete data.salesPersons
            delete data.customers

            const salespUrl = 'http://localhost:8090/api/sales/';
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data), // This is sending our data copy
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(salespUrl, fetchConfig);
            console.log(response)
            if (response.ok) {

                const cleared = {
                VINS: [],
                salesPersons: [],
                customers: [],
                price: '',
                };
                this.setState(cleared);
            }
        }

        // This is our universal handlechange function we can use for any
        //property listed above. No Bind needed, and don't need multiple handle changes
        handleChange=(event) => {
            const {name, value} = event.target;
            this.setState({ [name]: value })
        }



        render() {
            return (
                <div className='my-5' container='true'>
                        <div className="offset-3 col-6">
                            <div className="shadow p-4 mt-4">
                                <h1>Create Sale</h1>

                                {/* New Sale Form */}
                                <div className='col'>
                                    <div className='card shadow'>
                                    <div className='card-body'>
                                    <form className='form' onSubmit={(e) => this.handleSubmit(e)}> {/*We want to use an anonymous function for onSubmit because if not it will be called on ComponentMount*/}
                                        <div className='row'>
                                            <div className='col'>
                                                <div className="form-floating mb-3">
                                                    {/* Unsold Car Select */}
                                                    <select onChange={this.handleChange}  required name="VIN" id="VIN" className="form-select" value={this.state.VIN}>
                                                    <option> Choose an Automobile </option>
                                                    {this.state.VINS.map(Automobiles => {
                                                        return (
                                                            <option key={Automobiles.id} value={Automobiles.id}>
                                                                {Automobiles.VIN}
                                                            </option>
                                                        );
                                                    })}
                                                    </select>

                                                {/* Employee Name Select */}
                                                <select onChange={this.handleChange}  required name="salesPerson" id="salesPerson" className="form-select" value={this.state.salesPerson}>
                                                    <option> Choose a Sales Person </option>
                                                    {this.state.salesPersons.map(salesPerson => {
                                                        return (
                                                            <option key={salesPerson.id} value={salesPerson.id}>
                                                                {salesPerson.name}

                                                            </option>
                                                        );
                                                    })}
                                                    </select>

                                                    <select onChange={this.handleChange}  required name="customer" id="customer" className="form-select" value={this.state.customer}>
                                                    <option> Choose a Customer </option>
                                                    {this.state.customers.map(customer => {
                                                        return (
                                                            <option key={customer.id} value={customer.id}>
                                                                {customer.name}

                                                            </option>
                                                        );
                                                    })}
                                                    </select>

                                                        <div>
                                                        <input onChange={this.handleChange} placeholder="price" required
                                                            type="text" name="price" id="price"
                                                            className="form-control" value={this.state.price} />
                                                        <label htmlFor="price">  </label>
                                                    </div>
                                            <button id="submit-button" className="btn btn-primary">Create Sale</button>
                                            </div>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
};

export default SaleForm;
