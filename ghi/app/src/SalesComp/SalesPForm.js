// You need to create a form that allows a person to enter the name and employee number for a sales person.
// When the form is submitted, the sales person is created in the application.
// You need to create a link in the navbar to get to the Add a sales person form.
import React from 'react';

class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            employee_number: '',
        };
    }

        async handleSubmit(event) {
            event.preventDefault();

            const data = {...this.state} ; //copies all of the properties and values from this.sales into a new object (dictionary)


            const salespUrl = 'http://localhost:8090/api/sales/employees/';
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data), // This is sending our data copy
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(salespUrl, fetchConfig);
            console.log(response)
            if (response.ok) {

                const cleared = {
                    name: '',
                    employee_number: '',
                };
                this.setState(cleared);
            }
        }

        // This is our universal handlechange function we can use for any
        //property listed above. No Bind needed, and don't need multiple handle changes
        handleChange=(event) => {
            const {name, value} = event.target;
            this.setState({ [name]: value })
        }



        render() {
            return (
                <div className='my-5' container='true'>
                        <div className="offset-3 col-6">
                            <div className="shadow p-4 mt-4">
                                <h1>Create Employee</h1>

                                {/* New Employee Form */}
                                <div className='col'>
                                    <div className='card shadow'>
                                    <div className='card-body'>
                                        <form className='form' onSubmit={(e) => this.handleSubmit(e)}> {/*We want to use an anonymous function for onSubmit because if not it will be called on ComponentMount*/}
                                            <div className='row'>
                                                <div className='col'>
                                                    <div className="form-floating mb-3">
                                                        <input onChange={this.handleChange} placeholder="Name" required
                                                            type="text" name="name" id="name"
                                                            className="form-control" value={this.state.name} />
                                                        <label htmlFor="name">Name</label>
                                                    </div>
                                                    </div>

                                                </div>
                                                <div className="form-floating mb-3">
                                                        <input onChange={this.handleChange} placeholder="employee_number" required
                                                            type="text" name="employee_number" id="employee_number"
                                                            className="form-control" value={this.state.employee_number} />
                                                        <label htmlFor="employee_name">Employee ID</label>
                                                    </div>
                                            <button id="submit-button" className="btn btn-primary">Create Employee</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
    };

export default SalesPersonForm;
