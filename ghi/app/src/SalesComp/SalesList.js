// You need to show a page that lists all sales showing the sales person's
// name and salesPerson number, the purchaser's name, the automobile VIN, and the price of the sale.
// You need to create a link in the navbar to get to the list of all sales.

// Combine both Sales List with all Sales and the Sales Person select tag.
// When a specific Sales Person is selected we need to filter the page to list all that guys sales

// You need to show a list of the sales for a sales person's salesPerson number.
// To do this, create a page that has a dropdown that allows someone to choose a sales person from it. When the dropdown selection changes, fetch all of the sales associated with the sales person selected in the dropdown. Then, show that list of sales that has the sales person, the customer, the automobile VIN, and the price of the sale.
// You need to creanavbar-brandte a link in the navbar to get to the page that shows a specific sales person's sales history.
// The following screenshot shows what your component could look like.


import React, { Component } from 'react'
//import './SalesComp/App.css'

class SalesList extends Component {
      state = {
         salesList: [],
         salesPersons: [],
         filtered: [],
         salesPerson: '',
         sales: '',
         name: '',
         customer: '',
         salesPerson_number: '',
         customer: '',
         VIN: '',
         price: '',
      };


    handleChange = (event) => {
        // const {name, value} = event.target;
        // this.setState({[name]: Number(value)})
        this.filtered(event)
    }

    // For salesPerson Select
    async componentDidMount() {
        const employees = "http://localhost:8090/api/sales/employees/";
        const salesListUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(employees);
        const response2 = await fetch(salesListUrl);
        if(response.ok && response2.ok) {
            const listEmployees = await response.json();
            const salesListData = await response2.json();
            this.setState({
                salesPersons: listEmployees["salesPerson"],
            })
            this.setState({
                salesList: salesListData["sales"]
            })
            console.log(this.state.salesList)
            this.setState({
                filtered: salesListData.sales
            })
        }

    }

    filtered = (event) => {
        let employee_id = event.target.value
        console.log(this.state.salesList)
        let filteredList = this.state.salesList.filter(sale => (sale.salesPerson.id === Number(employee_id)))
        this.setState({
            filtered: filteredList
        })
        console.log("filtered list:", filteredList)
    }


  render() {
    return (
      <div className="app-container">
        <div className='my-5' container='true'>
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
        <body>
        <div className="mb-3">
            <form>
            <h1 className="form-name"> Select salesPerson </h1>
                <select onChange={this.handleChange} required name="salesPerson" id="salesPerson" className="form-select" value={this.state.salesPerson.id}>
                <option>Choose salesPerson</option>
                    {this.state.salesPersons.map(salesPerson => {
                        return (
                            <option key={salesPerson.id} value={salesPerson.id}>
                                {salesPerson.name}
                            </option>
                        );
                    })}
                </select>
                </form>
            </div>
            <h1> Sales List </h1>
        <table>
            <thead >
                <tr>
                    <th>Sales Person</th>
                    <th>Employee Number</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>

        <tbody>
            {this.state.filtered.map( (sales) => {
            return(
                <tr key = {sales.id}>
                    <td>{sales.salesPerson.name} </td>
                    <td>{sales.salesPerson.employee_number} </td>
                    <td>{sales.customer.name}</td>
                    <td>{sales.VIN.VIN} </td>
                    <td>{sales.price} </td>
                </tr>)
            })}

            </tbody>
        </table>
        </body>
      </div>
    </div>
    </div>
    </div>
    )
  }
}

export default SalesList;
