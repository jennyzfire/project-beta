import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success" id="navbar">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <NavLink className="nav-item" to="/Appointments/">Appointments</NavLink>
        <NavLink className="nav-item" to="/History/">Service History</NavLink>
        <NavLink className="nav-item" to="/Technicians/">Technicians</NavLink>
        <NavLink className="nav-item" to="/Inventory/">Inventory</NavLink>
        <NavLink className="nav-item" to="/Manufacturers/List/">Manufacturers</NavLink>
        <NavLink className="nav-item" to="/Automobile/Form/">Create Auotmobile</NavLink>
        <NavLink className="nav-item" to="/Sales/Person/Form/">Create Employee</NavLink>
        <NavLink className="nav-item" to="/Customer/Form/">Create Customer</NavLink>
        <NavLink className="nav-item" to="/Sale/Form/">Sales Form</NavLink>
        <NavLink className="nav-item" to="/Sales/List/">Sales List</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
      </div>
    </nav>
  )
}

export default Nav;
