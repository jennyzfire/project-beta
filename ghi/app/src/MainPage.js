import './App.css'
import barbie from './images/barbie.jpg'

function MainPage() {
  return (
    <div>
      <div>
      <h1 className="carcar-title">CarCar</h1>
      <p className="paragraphs">
            The premiere solution for automobile dealership
            management!
      </p>
      <img src={barbie}></img>
      </div>
    </div>
  );
}

export default MainPage;
